# README #

### What is this repository for? ###

* A simple scrapper for https://videx.comesconnected.com/
* Symfony Version: 4.4
* External libraries:
  * Symfony DOMCrawler
  * Symfony CssSelector

### How do I get set up? ###

* To install all the dependencies: composer install
* To run the scapper execute from project directory:
  * Windows: php bin\console scrap:videx:products
  * Unix: php bin/console scrap:videx:products
* The scrapping result is stored in public/json directory
* To run the testing:
  * Windows: php bin\console test:videx
  * Unix: php bin/console test:videx