<?php


namespace App\Command;


use App\Services\VidexScrapper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScrapVidexProductsCommand extends Command
{
    protected static $defaultName = "scrap:videx:products";

    protected CONST JSON_DIRECTORY = "public/json";

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $videxScrapperService = new VidexScrapper();
        $productOptionsArray = $videxScrapperService->scrapVidex();

        file_put_contents(self::JSON_DIRECTORY . "/videx_" . date("d_m_Y_H") . ".json", json_encode($productOptionsArray));

        return 0;
    }

}