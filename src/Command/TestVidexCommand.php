<?php


namespace App\Command;


use App\Services\VidexScrapper;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestVidexCommand extends Command
{
    protected static $defaultName = "test:videx";

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $videxScrapperService = new VidexScrapper();

        $this->testURL($videxScrapperService->getUrl());
        $productOptionsArray = $videxScrapperService->scrapVidex();

        $this->testProductsAvailability($productOptionsArray);
        $this->testProductValues($productOptionsArray);
    }

    /**
     * Check if Videx Website is available
     * @param $url
     */
    protected function testURL($url)
    {
        $headers = get_headers($url, 1);
        if ($headers[0] != "HTTP/1.0 200 OK") {
            throw new Exception("Videx website not available");
        } else {
            echo "[OK] Videx website available" . PHP_EOL;
        }
    }

    /**
     * Check if there are available product options
     * @param $productOptions
     */
    protected function testProductsAvailability($productOptions)
    {
        if (count($productOptions) == 0) {
            throw new Exception("Videx product options not available");
        } else {
            echo "[OK] Videx product options available" . PHP_EOL;
        }
    }

    /**
     * Check if there are no empty values after scrapping
     * @param $productOptions
     */
    protected function testProductValues($productOptions)
    {
        foreach ($productOptions as $productOption) {
            foreach ($productOption as $key => $value) {
                if (empty($value)) {
                    throw new Exception("Empty value for " . $key);
                }
            }
        }
        echo "[OK] Products values" . PHP_EOL;
    }
}