<?php


namespace App\Services;


use Symfony\Component\DomCrawler\Crawler;

class VidexScrapper
{
    protected CONST URL = "https://videx.comesconnected.com/";

    /**
     * Main scrapping method
     * @return array
     */
    public function scrapVidex()
    {
        $htmlContent = file_get_contents(self::URL);

        $crawler = new Crawler($htmlContent);

        $crawler = $crawler->filter('section[id="subscriptions"]'); //There are 2 sections at this moment: Monthly and Annual
        $productOptionsArray = [];
        foreach ($crawler->filter('section[id="subscriptions"]') as $sectionHTML) {
            $sectionCrawler = new Crawler($sectionHTML);

            $subscriptionType = $this->getSubscriptionType($sectionCrawler->filter("h2")->text());

            //Iterating through every product option
            foreach ($sectionCrawler->filter(".package") as $packageHTML) {
                $packageCrawler = new Crawler($packageHTML);

                $optionName = str_replace("Option ", "", $packageCrawler->filter("h3")->text());
                $limitMinutes = trim(str_replace("Mins", "", $optionName));

                $arrayLimitAndExtraPrices = $this->getLimitsAndExtraPrice($packageCrawler->filter(".package-name")->html());

                $arrayPriceAndCurrency = $this->getPriceAndCurrency(trim($packageCrawler->filter(".price-big")->text()));

                $extraInformation = $packageCrawler->filter(".package-data")->text();

                if ($packageCrawler->filter(".package-price p")->count()) {
                    $promotion = $packageCrawler->filter(".package-price p")->text();
                } else {
                    $promotion = "No promotion";
                }

                $productOptionsArray[] = [
                    "optionName" => $optionName,
                    "limitMinutes" => $limitMinutes,
                    "limitSMS" => $arrayLimitAndExtraPrices["limitSMS"],
                    "priceExtraMinute" => $arrayLimitAndExtraPrices["priceExtraMinute"],
                    "priceExtraSMS" => $arrayLimitAndExtraPrices["priceExtraSMS"],
                    "currency" => $arrayPriceAndCurrency["currency"],
                    "optionPrice" => $arrayPriceAndCurrency["price"],
                    "extraInformation" => htmlspecialchars($extraInformation),
                    "promotion" => htmlspecialchars($promotion),
                    "subscriptionType" => $subscriptionType
                ];
            }
        }

        return $productOptionsArray;
    }

    /**
     * Get the subscription type
     * @param $string
     * @return string
     */
    private function getSubscriptionType($string)
    {
        if (strpos($string, "Monthly") !== false) {
            $type = "monthly";
        } elseif (strpos($string, "Annual ") !== false) {
            $type = "annual";
        } else {
            $type = "other";
        }

        return $type;
    }

    /**
     * Get price and currency
     * @param $string
     * @return array
     */
    private function getPriceAndCurrency($string)
    {
        if (strpos($string, "£") !== false) {
            $currencyCode = "GBP";
        } elseif (strpos($string, "$") !== false) {
            $currencyCode = "USD";
        } elseif (strpos($string, "€") !== false) {
            $currencyCode = "EUR";
        } else {
            $currencyCode = "OTHER";
        }

        return [
            "currency" => $currencyCode,
            "price" => floatval(substr($string, 2))
        ];
    }

    /**
     * Get limit of SMS, extra price for every minute and SMS after reaching the limit
     * @param $string
     * @return array
     */
    private function getLimitsAndExtraPrice($string)
    {
        preg_match("/including (.*?) SMS/", $string, $matches);
        $limitSMS = $matches[1];

        preg_match("#\\((.*?)p /#", $string, $matches);
        $priceExtraMinute = $matches[1] / 100;

        preg_match("#and (.*?)p /#", $string, $matches);
        $priceExtraSMS = $matches[1] / 100;

        return [
            "limitSMS" => $limitSMS,
            "priceExtraMinute" => $priceExtraMinute,
            "priceExtraSMS" => $priceExtraSMS
        ];
    }

    /**
     * Get the URL (Testing purpose)
     * @return string
     */
    public function getUrl()
    {
        return self::URL;
    }
}